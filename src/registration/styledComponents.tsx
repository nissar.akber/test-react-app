import styled from 'styled-components';

export const Wrapper = styled.div`
  background: ${({ theme: { palette } }) => palette.mainbg[palette.mode]};
`;

export const FormContainer = styled.div`
  background: ${({ theme: { palette } }) => palette.formbg[palette.mode]};
`;
