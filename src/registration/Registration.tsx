import React from 'react';

import { FormContainer, Wrapper } from './styledComponents';

import RegistrationForm from '../components/RegistrationForm';
import { Divider, Link, Typography, useTheme } from '@mui/material';
import { Box } from '@mui/system';
import ThemeImage from '../components/ThemeImage';
import ThemeSwitch from '../components/ThemeSwitch';
import SocialMediaLogin from '../components/SocialMediaLogin/SocialMediaLogin';
import { RegistartionFormInputs } from '../components/RegistrationForm/RegistrationForm';

const Header = () => (
  <div className="flex justify-between">
    <Typography color="primary" sx={{ fontWeight: 'bold', fontSize: 12 }}>
      Travelguru
    </Typography>
    <ThemeSwitch />
  </div>
);

const Caption = () => (
  <div>
    <Typography sx={{ fontWeight: 'bold', fontSize: 24 }}>
      Sign into Travelguru
    </Typography>
    <div className="flex">
      <Typography variant="caption">Don't have an account?</Typography>
      <Box sx={{ m: 1 }} />
      <Link
        underline="none"
        color="primary"
        variant="caption"
        sx={{ fontWeight: 'bold', cursor: 'pointer' }}
      >
        Sign up
      </Link>
    </div>
  </div>
);

const Registration = () => {
  const theme = useTheme();

  const onFormSubmit = (data: RegistartionFormInputs) =>
    alert(JSON.stringify(data));

  return (
    <Wrapper theme={theme} className="flex h-screen overflow-auto">
      <div className="columns-1 w-full md:flex md:columns-2 m-auto md:w-7/8 max-w-[1024px]">
        <div className="w-3/5 hidden md:flex lg:w-3/5">
          <ThemeImage className="rounded-l-3xl" />
        </div>
        {/* TODO: bg gradient */}
        <FormContainer
          theme={theme}
          className="flex w-full p-[4%] rounded-3xl md:rounded-l-none ease-in-out md:w-1/2 md:py-[0] lg:w-2/5"
        >
          <div className="flex-col m-auto space-y-[4%]">
            <Header />
            <Divider />
            <Caption />
            <Divider />
            <RegistrationForm onSubmit={onFormSubmit} />
            <Divider />
            <Typography variant="subtitle2" className="text-center">
              {'- OR -'}
            </Typography>
            <SocialMediaLogin />
          </div>
        </FormContainer>
      </div>
    </Wrapper>
  );
};

export default Registration;
