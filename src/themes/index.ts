import { PaletteMode } from '@mui/material';
import { createTheme } from '@mui/material/styles';

export const getAppTheme = (mode: PaletteMode) =>
  createTheme({
    palette: {
      mode,
      primary: {
        main: '#fc1703',
        light: '#fc1703',
        dark: '#fc1703',
      },
      secondary: {
        main: '#d8dff7',
        light: '#d8dff7',
        dark: '#3d3d5c',
      },
      error: {
        main: '#c20202',
        light: '#c20202',
        dark: '#c20202',
      },
      mainbg: {
        main: '#d8dff7',
        light: '#d8dff7',
        dark: '#3d3d5c',
      },
      label: {
        main: '#0d0d0d',
        light: '#0d0d0d',
        dark: ' #f8f8f8',
      },
      formbg: {
        main: '#eaeefb',
        light: '#eaeefb',
        dark: '#180742',
      },
      socialIconsBg: {
        main: '#f2dfdf',
        light: '#f2dfdf',
        dark: ' #f8f8f8',
      },
    },
    typography: {
      button: {
        textTransform: 'none',
        fontWeight: 'bold',
      },
    },
    shape: {
      borderRadius: 8,
    },
    spacing: 8,
  });

declare module '@mui/material/styles' {
  interface Palette {
    mainbg: Palette['primary'];
    label: Palette['primary'];
    formbg: Palette['primary'];
    socialIconsBg: Palette['primary'];
  }
  interface PaletteOptions {
    mainbg: PaletteOptions['primary'];
    label: PaletteOptions['primary'];
    formbg: PaletteOptions['primary'];
    socialIconsBg: PaletteOptions['primary'];
  }
}
