import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from 'react-router-dom';

import Registration from '../registration';

const AppRootView = () => {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<Navigate to="/registration" replace />} />
          <Route path="/registration" element={<Registration />} />
        </Routes>
      </Router>
    </>
  );
};

export default AppRootView;
