import { setMode } from '../../features/theme/themeSlice';
import { useAppDispatch, useAppSelector } from '../../root/hooks';
import { SwitchComponent } from './styledComponents';

const ThemeSwitch = () => {
  const mode = useAppSelector((state) => state.theme.mode);
  const isDarkMode = mode === 'dark';
  const dispatch = useAppDispatch();

  const handleChange = () => {
    dispatch(setMode(isDarkMode ? 'light' : 'dark'));
  };

  return <SwitchComponent checked={isDarkMode} onChange={handleChange} />;
};

export default ThemeSwitch;
