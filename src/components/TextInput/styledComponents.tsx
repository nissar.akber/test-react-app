import { FilledInput, styled } from '@mui/material';

export const InputField = styled(FilledInput)`
  border-radius: ${({ theme: { shape } }) => shape.borderRadius}px;
  background-color: ${({ theme: { palette } }) =>
    palette.secondary[palette.mode]} !important;
  font-size: 12px;
`;
