import {
  FormControl,
  OutlinedInputProps,
  useTheme,
  FormHelperText,
} from '@mui/material';
import { InputField } from './styledComponents';

export type TextInputProps = OutlinedInputProps & {
  errorMessage?: string;
};

const TextInput = (props: TextInputProps) => {
  const theme = useTheme();
  const { errorMessage, ...inputProps } = props;
  return (
    <FormControl variant="outlined" className="flex w-full">
      <InputField
        theme={theme}
        color="secondary"
        disableUnderline
        hiddenLabel
        size="small"
        {...inputProps}
      />
      <FormHelperText error={true}>{errorMessage}</FormHelperText>
    </FormControl>
  );
};

export default TextInput;
