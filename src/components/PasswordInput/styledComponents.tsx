import { FilledInput, styled } from '@mui/material';

export const PasswordField = styled(FilledInput)`
  border-radius: ${({ theme: { shape } }) => shape.borderRadius}px;
  background-color: ${({ theme: { palette } }) =>
    palette.secondary[palette.mode]} !important;
  font-size: 12px;
  webkit-box-shadow: 0 0 0 1000px red inset;
`;
