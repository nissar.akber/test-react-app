import { Visibility, VisibilityOff } from '@mui/icons-material';
import {
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  OutlinedInputProps,
} from '@mui/material';
import { useState } from 'react';
import { PasswordField } from './styledComponents';

export type PasswordInputProps = OutlinedInputProps & {
  errorMessage?: string;
};

const PasswordInput = (props: PasswordInputProps) => {
  const [showPassword, setShowPassword] = useState(false);
  const { errorMessage, ...inputProps } = props;

  return (
    <FormControl variant="outlined" className="flex w-full">
      <PasswordField
        color="secondary"
        disableUnderline
        hiddenLabel
        size="small"
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label={`${showPassword ? 'show' : 'hide'} password`}
              onClick={() => setShowPassword(!showPassword)}
              onMouseDown={(event: React.MouseEvent<HTMLButtonElement>) => {
                event.preventDefault();
              }}
              edge="end"
            >
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          </InputAdornment>
        }
        type={showPassword ? 'text' : 'password'}
        {...inputProps}
      />
      <FormHelperText error={true}>{errorMessage}</FormHelperText>
    </FormControl>
  );
};

export default PasswordInput;
