import { Button } from '@mui/material';
import { useForm, Controller, SubmitHandler } from 'react-hook-form';

import PasswordInput from '../PasswordInput';
import TextInput from '../TextInput';

export interface RegistartionFormInputs {
  fullname: string;
  email: string;
  password: string;
}

const formOptions = {
  defaultValues: {
    fullname: '',
    email: '',
    password: '',
  },
};

const validationRules = {
  fullname: { required: 'Please enter your name' },
  email: {
    required: 'Please enter your email',
    pattern: {
      value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
      message: 'Invalid email address',
    },
  },
  password: {
    required: 'Please enter a password',
    minLength: {
      value: 8,
      message: 'Must be minumum eight characters',
    },
  },
};

export type RegistrationFormProps = {
  onSubmit?: (data: RegistartionFormInputs) => void;
};

const RegistrationForm = (props: RegistrationFormProps) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<RegistartionFormInputs>(formOptions);
  const onSubmit: SubmitHandler<RegistartionFormInputs> = (data) =>
    props.onSubmit && props.onSubmit(data);

  return (
    <form
      autoComplete="off"
      className="flex-col space-y-[4%]"
      onSubmit={handleSubmit(onSubmit)}
    >
      <Controller
        name="fullname"
        control={control}
        rules={validationRules.fullname}
        render={({ field: { onChange, value } }) => (
          <TextInput
            id="fullname"
            type="text"
            placeholder="Full name"
            onChange={onChange}
            value={value}
            error={errors.fullname && true}
            errorMessage={errors.fullname?.message}
          />
        )}
      />
      <Controller
        name="email"
        control={control}
        rules={validationRules.email}
        render={({ field: { onChange, value } }) => (
          <TextInput
            id="email"
            type="text"
            placeholder="Email"
            autoComplete="off"
            onChange={onChange}
            value={value}
            error={errors.email && true}
            errorMessage={errors.email?.message}
          />
        )}
      />
      <Controller
        name="password"
        control={control}
        rules={validationRules.password}
        render={({ field: { onChange, value } }) => (
          <PasswordInput
            id="new-password"
            placeholder="Password"
            onChange={onChange}
            value={value}
            error={errors.password && true}
            errorMessage={errors.password?.message}
          />
        )}
      />

      <Button
        variant="contained"
        color="primary"
        className="flex w-full"
        type="submit"
      >
        Continue
      </Button>
    </form>
  );
};

export default RegistrationForm;
