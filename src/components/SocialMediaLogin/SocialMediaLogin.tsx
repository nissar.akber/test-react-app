import {
  Apple as AppleLogo,
  Facebook as FacebookLogo,
} from '@mui/icons-material';
import { Button, Icon, IconButton, useTheme } from '@mui/material';
import { useMemo } from 'react';

import GoogleLogo from '../../assets/images/logo_google.svg';

const SocialMediaLogin = () => {
  const GoogleIcon = (
    <Icon>
      <img alt="edit" src={GoogleLogo} />
    </Icon>
  );

  const { palette } = useTheme();
  const sx = useMemo(
    () => ({
      backgroundColor: palette.socialIconsBg[palette.mode],
      color: '#0d0d0d',
      '&:hover': {
        backgroundColor: palette.socialIconsBg[palette.mode],
        color: '#0d0d0d',
      },
      borderRadius: 1,
    }),
    [palette.mode, palette.socialIconsBg],
  );

  return (
    <div className="flex w-full space-between space-x-[5%]">
      <Button
        size="small"
        className="flex w-[60%]"
        startIcon={GoogleIcon}
        sx={sx}
      >
        Sign in with Google
      </Button>
      <IconButton size="small" className="flex w-[15%]" sx={sx}>
        <FacebookLogo />
      </IconButton>
      <IconButton size="small" className="flex w-[15%]" sx={sx}>
        <AppleLogo />
      </IconButton>
    </div>
  );
};
export default SocialMediaLogin;
