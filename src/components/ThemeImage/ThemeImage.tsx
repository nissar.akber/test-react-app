import { useTheme } from '@mui/material';

import themeImgLight from '../../assets/images/theme_image_light.jpg';
import themeImgDark from '../../assets/images/theme_image_dark.jpg';

const themeImages = {
  light: themeImgLight,
  dark: themeImgDark,
};
const ThemeImage = (props: any) => {
  const {
    palette: { mode },
  } = useTheme();

  return <img src={themeImages[mode]} alt="theme" {...props} />;
};

export default ThemeImage;
