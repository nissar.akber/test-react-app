import React, { useMemo } from 'react';
import { Provider as StateProvider } from 'react-redux';
import { CssBaseline, ThemeProvider } from '@mui/material';

import AppRootView from './root';
import { getAppTheme } from './themes';
import { store } from './root/store';

import './App.css';
import { useAppSelector } from './root/hooks';

const ThemedApp = () => {
  const themeMode = useAppSelector((state) => state.theme.mode);
  const theme = useMemo(() => getAppTheme(themeMode), [themeMode]);
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppRootView />
    </ThemeProvider>
  );
};

function App() {
  return (
    <StateProvider store={store}>
      <ThemedApp />
    </StateProvider>
  );
}

export default App;
